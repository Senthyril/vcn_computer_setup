#! /bin/bash

#unpacks compressed profile file and makes a new profile using the configuration stored in it.
sudo tar xf vcn_profile_firefox.tar.xz -C /usr/local/bin/firefox_profile
firefox -CreateProfile "VCN /usr/local/bin/firefox_profile"

#install chrome
sudo wget -T 10 -nd -nc https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb

#add chrome shortcut
sudo cp google-chrome.desktop ~/.local/share/applications/google-chrome.desktop

#install gimp
sudo add-apt-repository ppa:otto-kesselgulasch/gimp
sudo apt update
sudo apt-get -y install gimp
