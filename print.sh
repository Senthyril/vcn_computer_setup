#! /bin/bash

#-------------------- beginning-------------------

#printer setting up
#create a directory to download the packages
mkdir -p brother-drivers/mfc-9970cdw
cd brother-drivers/mfc-9970cdw

#install necessary libraries for printer
apt-get install ia32-libs
apt-get install lib32stdc++6

#download the packages for mfc9970cdw
sudo wget -T 10 -nd -nc http://www.brother.com/pub/bsc/linux/dlf/mfc9970cdwlpr-1.1.1-5.i386.deb
sudo wget -T 10 -nd -nc http://www.brother.com/pub/bsc/linux/dlf/mfc9970cdwcupswrapper-1.1.1-5.i386.deb

#download the scan packages
#sudo wget -T 10 -nd -nc http://www.brother.com/pub/bsc/linux/packages/brscan-0.2.4-0.amd64.dsudo -H gedit /var/lib/polkit-1/localauthority/10-vendor.d/com.ubuntu.desktop.pklaeb
#sudo wget -T 10 -nd -nc http://www.brother.com/pub/bsc/linux/packages/brscan-skey-0.2.4-1.amd64.deb	

#install the printer driver
sudo dpkg -i --force-all  mfc9970cdwlpr-1.1.1-5.i386.deb
sudo dpkg -i --force-all mfc9970cdwcupswrapper-1.1.1-5.i386.deb

#restart cups service
sudo systemctl restart cups.service

#configure printer parameters and ip address
sudo lpadmin -p 'MFC9970CDW' -v socket://192.168.20.51 -E





